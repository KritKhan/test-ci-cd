FROM node:latest

WORKDIR ./app/

COPY package.json .

COPY yarn.lock .

RUN yarn global add serve
RUN yarn install

COPY . .

EXPOSE 3000

RUN yarn build

CMD ["serve","-p", "3000", "-s", "./build"]
